import React from 'react';
import {View, Text, Pressable, StyleSheet} from 'react-native';
import {secondary} from '../config/colors';

const GetStarted = props => {
  const onPressButton = e => {
    props.onPress();
  };
  return (
    <View style={props.style}>
      <Pressable style={styles.button} onPress={onPressButton}>
        <Text style={styles.text}>Get Started</Text>
      </Pressable>
    </View>
  );
};

const styles = StyleSheet.create({
  button: {
    backgroundColor: secondary,
    width: '90%',
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 10,
    shadowColor: 'black',
    shadowOffset: '10',
    borderRadius: 10,
  },
  text: {
    color: 'black',
    fontSize: 24,
    fontFamily: 'Roboto-Black',
  },
});

export default GetStarted;
