import React from 'react';
import {Text, View, StyleSheet, StatusBar} from 'react-native';
import {primary, secondary} from '../config/colors';

const Welcome = props => {
  const texts = props.texts;
  return (
    <View style={props.style}>
      {texts[0] && <Text style={styles.text}>{texts[0]}</Text>}
      {texts[1] && (
        <Text
          style={{...styles.text, fontSize: 36, fontFamily: 'Roboto-light'}}>
          {texts[1]}
        </Text>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    fontSize: 48,
    fontFamily: 'Roboto-Bold',
    alignSelf: 'center',
    justifyContent: 'center',
    color: 'black',
    margin: 8,
  },
});

export default Welcome;
