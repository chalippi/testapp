import React, {useState, useEffect} from 'react';
import {View, StyleSheet, TextInput} from 'react-native';
import {primary, secondary} from '../config/colors';

const Input = props => {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');

  useEffect(() => {
    props.onChangeName({firstName, lastName});
  }, [firstName, lastName]);

  return (
    <View style={props.style}>
      <TextInput
        style={styles.input}
        onChangeText={setFirstName}
        value={firstName}
        placeholder="First Name"
        placeholderTextColor={'black'}
      />
      <TextInput
        style={styles.input}
        onChangeText={setLastName}
        value={lastName}
        placeholder="Last Name"
        placeholderTextColor={'black'}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1.25,
    borderRadius: 4,
    padding: 10,
    fontFamily: 'Roboto-Bold',
    color: 'black',
  },
});

export default Input;
