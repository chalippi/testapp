import React from 'react';
import {View, StyleSheet, Text} from 'react-native';
import {primary, secondary} from '../config/colors';

const TextArea = props => {
  const name = props.name;
  return (
    <View style={props.style}>
      {name != '' && (
        <Text
          style={{
            ...styles.text,
            margin: 20,
            padding: 20,
            borderRadius: 20,
            backgroundColor: secondary,
            color: 'black',
          }}>
          {`Hello! My name is `}
          <Text
            style={{
              ...styles.text,
              color: primary,
              marginVertical: 10,
            }}>          f{` ${name} `}</Text>
          <Text
            style={{
              ...styles.text,
            }}>{`nice to meet you :)`}</Text>
        </Text>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  text: {fontFamily: 'Roboto-Black', fontSize: 24},
});

export default TextArea;
