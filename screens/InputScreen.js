import React from 'react';
import {View, StyleSheet, Text} from 'react-native';
import Welcome from '../components/Welcome';
import {primary, secondary} from '../config/colors';
import Input from '../components/Input';
import TextArea from './TextArea';
import {useState} from 'react/cjs/react.development';

const InputScreen = props => {
  const texts = [null, 'Let me know you!'];

  const [name, setName] = useState('');

  const handleChangeName = ({firstName, lastName}) => {
    setName(`${firstName} ${lastName}`);
  };
  return (
    <View style={styles.container}>
      <Welcome style={styles.welcome} texts={texts} />
      <TextArea style={styles.textarea} name={name} />
      <Input style={styles.input} onChangeName={handleChangeName} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: primary,
  },
  welcome: {
    flex: 2,
    justifyContent: 'center',
  },
  input: {
    flex: 2,
    justifyContent: 'center',
  },
  textarea: {
    flex: 1,
    justifyContent: 'center',
  },
});

export default InputScreen;
