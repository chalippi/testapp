import React from 'react';
import {Image, View, StyleSheet, StatusBar} from 'react-native';
import {primary, secondary} from '../config/colors';
import Welcome from '../components/Welcome';
import GetStarted from '../components/GetStarted';

const WelcomeScreen = props => {
  const handlePress = e => {
    props.navigation.navigate('Input');
  };

  const texts = ['Hi !', 'Welcome to'];
  return (
    <View style={styles.container}>
      <StatusBar
        animated={true}
        backgroundColor={primary}
        barStyle={'dark-content'}
        showHideTransition={'slide'}
      />
      <Welcome style={styles.welcome} texts={texts} />
      <Image
        style={styles.image}
        source={require('../assets/images/logo.png')}
      />
      <GetStarted style={styles.get_started} onPress={handlePress} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: primary,
    flex: 1,
  },
  welcome: {
    flex: 2,
    justifyContent: 'center',
  },
  image: {
    resizeMode: 'contain',
    flex: 1,
  },
  get_started: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 2,
  },
});

export default WelcomeScreen;
